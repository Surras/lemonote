import {Meta} from '@storybook/angular/types-6-0';

import {MyButtonComponent} from './my-button.component';

export default {
  title: 'Components/Button',
  component: MyButtonComponent,
} as Meta;
